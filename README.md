# blr: Boilerplate
Create boilerplate starter files for computer languages.

## Compilation and Installation
1. Clone this repository (and cd into it):
```
git clone https://github.com/nswerhun/blr.git
cd blr
```
2. Install/Compile
    1. To compile only:
    ```
    make
    ```
    2. To compile and install to /usr/local/bin:
    ```
    sudo make install
    ```

## Usage
### RUN `--help`
```
blr [COMMAND] [LANG] [FILENAME]
```
COMMAND:
- `-w` write a file
- `-e` write an executable file (for scripts)

LANG (natively supported):
- `c`
- `cpp`
- `shell`
- `tex`
- `html`

\*note: lang names are case insensitive

If FILENAME already exists, it will be overwritten. Be careful. 

## Adding/changing/removing templates
I tried my best to make changing templates as easy as possible. This is subject to change in the future.

To modify templates, open `language.h`. You will see that each language is a struct in an array, with members `name` and `template`. To add another language, simply add another struct to the array. Lets add a template for the Java programming language.
```
        "    <body>\n"
        "        <++>\n"
        "    </body>\n"
        "</html>"
    },
    {
        "java",
        "public class Main {\n"
        "    public static void main(String[] args) {\n"
        "        <++>\n"
        "    }\n"
        "}"
    }
};
```
We can see that we have added another struct. The `name` is `java`. This will be the name you call it by in LANG at runtime. The next member is a string which is the template. It is that simple. Do not forget to add a comma after the previous entry if it is not there already.

To use our new template, we first must compile: `make` (or `sudo make install`). Then run the program like so:
```
blr -w java Main.java
```
You can open `Main.java` and see that your template was successful.

With this system, you can easily add/modify/remove templates as you please quickly and easily.