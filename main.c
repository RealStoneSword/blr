#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <sys/stat.h>
#include "languages.h"

const long int num_of_langs = sizeof(languages)/sizeof(*languages);
const char *short_args = "we";

void die(char *str) {
    fprintf(stderr, "error: %s\n", str);
    exit(EXIT_FAILURE);
}

void help(void) {
    printf("blr [COMMAND] [LANG] [FILENAME]\n");
    printf("\n");
    printf("COMMAND:\n");
    printf("  -w write a file\n");
    printf("  -e write an executable file (for scripts)\n");
    printf("LANG:\n");
    for (int i = 0; i < num_of_langs; i++) {
        printf("  %s", languages[i].name);
        if ((i+1)%3 == 0) {
            putchar('\n');
        } else if ((i+1) != num_of_langs) {
            putchar('\t');
        } else {
            putchar('\n');
        }
    }
    printf("*note: lang names are case insensitive\n");
    printf("\n");
    printf("If FILENAME already exists, it will be overwritten. Be careful.\n");
    exit(0);
}

void write(FILE **pntr, char *lang) {
    for (int i = 0; i < num_of_langs; i++){
        if (strcasecmp(lang, languages[i].name) == 0)
            fprintf(*pntr, "%s", languages[i].template);
    }
}

int string_in_langs(char *value) {
    for (int i = 0; i < num_of_langs; i++) {
        if (strcasecmp(value, languages[i].name) == 0)
            return 0;
    }
    return 1;
}

int char_in_args(char value, int string_length) {
    for (int i = 0; i < string_length; i++) {
        if (value == short_args[i])
            return 0;
    }
    return 1;
}

int main(int argc, char *argv[]) {
    FILE *fp;

    /* Usage checking */
    if (argc == 1)
        die("missing arguments\nTry --help");
    if (strcmp(argv[1], "--help") == 0 )
        help();
    if (char_in_args(argv[1][1], 2) == 1 || argv[1][0] != '-')
        die("invalid argument\nTry --help");
    if (argc == 2)
        die("missing arguments\nTry --help");
    if (string_in_langs(argv[2]) == 1)
        die("invalid or unsupported language\nTry --help");

    /* Open file pointer */
    fp = fopen(argv[3], "w");
    if (fp == NULL)
        die("failed to open file (null pointer)");

    /* Parsing of command */
    switch (argv[1][1]) {
        case 'w':
            write(&fp, argv[2]);
            break;
        case 'e':
            write(&fp, argv[2]);
            chmod(argv[3], 0775);
            break;
    }

    /* Close file pointer */
    fclose(fp);

    return 0;
}
